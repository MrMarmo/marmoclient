import com.google.gson.*;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

public class ClientTest extends BaseTest {

    @Test(groups = {"getresponse"})
    public void getResponse() {
        MarmoRequest request = new MarmoRequest();
        request.setUrl("http://maps.googleapis.com/maps/api/geocode/json");
        request.setParameters("latlng=40.714224,-73.961452");
        request.setRequestType("GET");
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        request.setHeaders(headers);
        MarmoResponse response = doRequest(request);
        System.out.print("Response Code: " + response.getResponseCode() + "\nResponse Message: " + response.getResponseMessage() + "\nResponse Body: " + response.getResponseBody());
    }

    @Test(groups = {"checkresponsecode"})
    public void checkResponseCode() {
        MarmoResponse response = doRequest();
        Assert.assertEquals(response.getResponseCode(), 200);
        System.out.println("Response Code: " + response.getResponseCode());
    }

    @Test(groups = {"checklongname"})
    public void checkLongName() {
        String longName = "";
        MarmoResponse response = doRequest();
        JsonObject jsonResponse=response.getResponseAsJsonObject();
        JsonElement results=getJsonElement(jsonResponse,"results");
        if(results!=null){
            for(JsonElement result:results.getAsJsonArray()){
                if(!longName.isEmpty())break;
                JsonElement addressComponents=getJsonElement(result.getAsJsonObject(),"address_components");
                if(addressComponents!=null){
                    for(JsonElement address:addressComponents.getAsJsonArray()){
                        JsonObject component=address.getAsJsonObject();
                        JsonElement types=getJsonElement(component,"types");
                        if(types!=null && types.toString().contains("administrative_area_level_1"))longName=component.get("long_name").getAsString();
                    }
                }
            }
        }
        System.out.println(longName);
        Assert.assertEquals(longName, "New York");
    }

    @Test(groups = {"checkshortname"})
    public void checkShortName() {
        String shortName = "";
        MarmoResponse response = doRequest();
        JsonObject jsonResponse=response.getResponseAsJsonObject();
        JsonElement results=getJsonElement(jsonResponse,"results");
        if(results!=null){
            for(JsonElement result:results.getAsJsonArray()){
                if(!shortName.isEmpty())break;
                JsonElement addressComponents=getJsonElement(result.getAsJsonObject(),"address_components");
                if(addressComponents!=null){
                    for(JsonElement address:addressComponents.getAsJsonArray()){
                        JsonObject component=address.getAsJsonObject();
                        JsonElement types=getJsonElement(component,"types");
                        if(types!=null && types.toString().contains("administrative_area_level_1"))shortName=component.get("short_name").getAsString();
                    }
                }
            }
        }
        System.out.println(shortName);
        Assert.assertEquals(shortName, "NY");
    }

    @Test(groups = {"checkformattedaddress"})
    public void checkFormattedAddress() {
        String formattedAddress = "";
        MarmoResponse response = doRequest();
        JsonObject jsonResponse=response.getResponseAsJsonObject();
        JsonElement results=getJsonElement(jsonResponse,"results");
        if(results!=null){
            for(JsonElement result:results.getAsJsonArray()){
                if(!formattedAddress.isEmpty())break;
                JsonElement address=getJsonElement(result.getAsJsonObject(),"formatted_address");
                if(address!=null)formattedAddress=address.getAsString();
            }
        }
        System.out.println(formattedAddress);
        Assert.assertEquals(formattedAddress, "277 Bedford Ave, Brooklyn, NY 11211, ES");
    }
}
