import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.sun.deploy.net.HttpUtils;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Created by Marmo on 14/09/2016.
 */
public class TwitterTest extends BaseTest {
    @Test(groups={"searchTests"})
    public void getLastNasaTweet(){
        MarmoRequest request=new MarmoRequest();
        request.setUrl(getURLFromConfig("Twitter Search"));
        request.setRequestType("GET");
        try {
            request.setParameters("q="+URLEncoder.encode("from:NASA","UTF-8")+"&lang=en");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        MarmoResponse response=doTwitterSearchRequest(request);
        JsonObject jsonResponse= response.getResponseAsJsonObject();
        JsonElement tweets=getJsonElement(jsonResponse,"statuses");
        String lastTweetText="";
        if(tweets!=null && tweets.isJsonArray()){
            for(JsonElement tweet:tweets.getAsJsonArray()){
                if(!lastTweetText.isEmpty())
                    break;
                JsonElement text=getJsonElement(tweet.getAsJsonObject(),"text");
                if(text!=null)lastTweetText=text.getAsString();
            }
        }
        System.out.println("Last NASA tweet was: "+lastTweetText);
    }
    @Test(groups={"searchTests"})
    public void getMostTrendingTopicInSpain(){
        MarmoRequest request=new MarmoRequest();
        request.setUrl(getURLFromConfig("Twitter Trends"));
        request.setRequestType("GET");
        request.setParameters("id=23424950");
        MarmoResponse response=doTwitterSearchRequest(request);
        JsonElement jsonResponse=response.getResponseAsJsonElement();
        if(jsonResponse!=null && jsonResponse.isJsonArray()){
            for(JsonElement element:jsonResponse.getAsJsonArray()){
                JsonElement trendsElement=getJsonElement(element.getAsJsonObject(),"trends");
                if(trendsElement!=null && trendsElement.isJsonArray()){
                    JsonObject highestVolumeTrend=null;
                    int highestVolume=0;
                    for(JsonElement trend:trendsElement.getAsJsonArray()){
                        JsonElement tweetVolume=getJsonElement(trend.getAsJsonObject(),"tweet_volume");
                        if(tweetVolume!=null && tweetVolume.getAsInt()>highestVolume){
                            highestVolume=tweetVolume.getAsInt();
                            JsonElement trendName=getJsonElement(trend.getAsJsonObject(),"name");
                            if(trendName!=null){
                                System.out.println(trendName.getAsString());
                                return;
                            }
                        }
                    }
                }
            }
        }
    }
}
