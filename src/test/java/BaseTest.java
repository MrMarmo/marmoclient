import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Base64;


/**
 * Created by Marmo on 31/08/2016.
 */
public class BaseTest {
    public MarmoClient client = new MarmoClient();

    public MarmoResponse doRequest(MarmoRequest request) {
        return client.set(request);
    }

    public MarmoResponse doTwitterSearchRequest(MarmoRequest request){
        MarmoRequest authRequest=new MarmoRequest();
        authRequest.setUrl(getURLFromConfig("Twitter Auth"));
        String auth="";
        try {
            auth= URLEncoder.encode("cRkuX90JPKD1ROpYkbcVD9Acs","UTF-8")+":"+URLEncoder.encode("DYav3POYf1nVPYyLVadgFXMyiAM4jpEneFzeYd6AI9Pj83fwqx","UTF-8");

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        if(!auth.isEmpty()){

            authRequest.setRequestType("POST");
            authRequest.setParameters("grant_type=client_credentials");
            Map<String, String> headers = new HashMap<>();
            try {
                headers.put("Authorization", "Basic "+Base64.getUrlEncoder().encodeToString(auth.getBytes("UTF-8")));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            headers.put("Content-Type","application/x-www-form-urlencoded;charset=UTF-8");
            authRequest.setHeaders(headers);
            MarmoResponse response=doRequest(authRequest);
            JsonObject jsonResponse= response.getResponseAsJsonObject();
            JsonElement element=getJsonElement(jsonResponse,"token_type");
            String token="";
            if(element!=null && element.getAsString().equals("bearer"))
                token=getJsonElement(jsonResponse,"access_token").getAsString();
            if(!token.isEmpty()){
                Map<String, String> reqHeaders = new HashMap<>();
                reqHeaders.put("Content-Type","application/x-www-form-urlencoded;charset=UTF-8");
                reqHeaders.put("Authorization","Bearer "+token);
                request.setHeaders(reqHeaders);
            }
        }
        return client.set(request);
    }
    public MarmoResponse doRequest() {
        MarmoRequest request = new MarmoRequest();
        request.setUrl(getURLFromConfig("Geocode"));
        request.setParameters("latlng=40.714224,-73.961452");
        request.setRequestType("GET");
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        request.setHeaders(headers);
        return client.set(request);
    }

    public JsonElement getJsonElement(JsonObject json,String element){
        JsonElement returnElement=json.get(element);
        if(returnElement!=null && !returnElement.isJsonNull())return returnElement;
        return null;
    }
    protected String getURLFromConfig(String type) {
        String url="";
        if(type.equals("Geocode"))url="geocodeUrl";
        else if(type.equals("Twitter Search"))url="twitterSearchUrl";
        else if(type.equals("Twitter Auth"))url="twitterOauthTokenUrl";
        else if(type.equals("Twitter Trends"))url="twitterTrendsUrl";
        Properties properties = new Properties();
        InputStream stream = getClass().getClassLoader().getResourceAsStream("config.properties");
        String configURL = "";
        if (stream != null) {
            try {
                properties.load(stream);
                configURL = properties.getProperty(url);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return configURL;
    }
}
