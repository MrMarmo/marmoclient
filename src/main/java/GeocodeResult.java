import com.google.gson.annotations.SerializedName;

public class GeocodeResult {
    @SerializedName("address_components")
    public GeocodeAddressComponents[] addressComponents;
    @SerializedName("formatted_address")
    public String formattedAddress;
    public GeocodeGeometry geometry;
    @SerializedName("place_id")
    public String placeID;
    public String[] types;
}
