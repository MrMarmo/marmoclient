import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

public class MarmoClient {

    public URL url;
    public HttpURLConnection connection;


    public MarmoResponse set(MarmoRequest request) {
        MarmoResponse response = new MarmoResponse();
        String requesType = request.getRequestType();
        try {
            if (requesType.equals("GET") || requesType.equals("DELETE"))
                url = new URL(request.getUrl() + "?" + request.getParameters());
            else if (requesType.equals("POST") || requesType.equals("PUT")) {
                url = new URL(request.getUrl());
            }
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod(request.getRequestType());
            for (Map.Entry<String, String> entry : request.getHeaders().entrySet()) {
                connection.setRequestProperty(entry.getKey(), entry.getValue());
            }
            if (requesType.equals("POST") || requesType.equals("PUT")) {
                connection.setDoOutput(true);
                DataOutputStream writer = new DataOutputStream(connection.getOutputStream());
                writer.writeBytes(request.getParameters());
                writer.flush();
                writer.close();
            }
            response.setResponseCode(connection.getResponseCode());
            response.setResponseMessage(connection.getResponseMessage());
            BufferedReader reader;
            if (response.getResponseCode() >= 200 && response.getResponseCode() < 300)
                reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            else reader = new BufferedReader((new InputStreamReader(connection.getErrorStream())));
            StringBuilder content = new StringBuilder();
            String input;
            while ((input = reader.readLine()) != null) {
                content.append(input);
            }
            reader.close();
            response.setResponseBody(content.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

}
