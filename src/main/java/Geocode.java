import com.google.gson.annotations.SerializedName;

/**
 * Created by SYSTEM on 27/08/2016.
 */
public class Geocode {
    @SerializedName("results")
    public GeocodeResult[] geocodeResults;
    public String status;

    public Geocode() {
    }
}
