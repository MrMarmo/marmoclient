/**
 * Created by SYSTEM on 27/08/2016.
 */
public class GeocodeGeometry {
    public GeocodeLatLon location;
    public String locationType;
    public GeocodeViewport viewport;
    public GeocodeViewport bounds;
}
