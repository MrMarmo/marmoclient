import java.util.Map;

/**
 * Created by Marmo on 31/08/2016.
 */
public class MarmoRequest {
    private String url;
    private String parameters;
    private String requestType;
    private Map<String, String> headers;

    public void setUrl(String url) {
        this.url = url;
    }

    public void setParameters(String parameters) {
        this.parameters = parameters;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }

    public String getUrl() {
        return url;
    }

    public String getParameters() {
        return parameters;
    }

    public String getRequestType() {
        return requestType;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }
}
