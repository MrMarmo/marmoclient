import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/**
 * Created by Marmo on 31/08/2016.
 */
public class MarmoResponse {
    private int responseCode;
    private String responseMessage;
    private String responseBody;

    public void setResponseCode(int code) {

        responseCode = code;
    }

    public void setResponseMessage(String message) {

        responseMessage = message;
    }

    public void setResponseBody(String body) {

        responseBody = body;
    }

    public int getResponseCode() {

        return responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }
    public String getResponseBody() {

        return responseBody;
    }
    public JsonObject getResponseAsJsonObject(){
        return new Gson().fromJson(getResponseBody(),JsonObject.class);
    }
    public JsonElement getResponseAsJsonElement(){
        return new Gson().fromJson(getResponseBody(),JsonElement.class);
    }
}
